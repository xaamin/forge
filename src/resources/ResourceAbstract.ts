import TransformerAbstract from "../TransformerAbstract";

/**
 * ResourceAbstract class
 *
 * @class ResourceAbstract
 * @constructor
 */
class ResourceAbstract {
  data: any | unknown[];
  meta: { [k: string]: unknown } | null;
  transformer: TransformerAbstract | null;
  variant: string | null;
  pagination: { [k: string]: unknown } | null;
  resourceKey: string | null;

  /**
   * Constractor for the ResourceAbstract
   * This allowes to set data and transformer while creating an instance
   */
  constructor(data: any |unknown[], trans: TransformerAbstract | null = null, resourceKey: string | null) {
    this.data = data;
    this.meta = null;

    const { transformer, variant } = this._separateTransformerAndVariation(trans);

    this.transformer = transformer;
    this.variant = variant;
    this.pagination = null;
    this.resourceKey = resourceKey;
  }

  /**
   * Return the data for this resource
   */
  async getData() {
    // data can be a promise, so we wait until it resolves
    const data = await this.data;

    // data is an item, so we return it as is
    return data;
  }

  /**
   * Returns the transformer set for this resource
   */
  getTransformer() {
    return this.transformer;
  }

  /**
   * Set Meta data that will be included when transforming this resource
   *
   * @param {Object} meta
   */
  setMeta(meta: { [k: string]: unknown }) {
    this.meta = meta;

    return this;
  }

  /**
   * Returns the metadata
   */
  getMeta() {
    return this.meta;
  }

  /**
   * Set pagination information for this resource
   *
   * @param {Object} pagination
   */
  setPagination(pagination: { [k: string]: unknown }) {
    this.pagination = pagination;

    return this;
  }

  /**
   * Returns the saved pagination information
   */
  getPagination() {
    return this.pagination;
  }

  /**
   * Set the transformer variant to be used for this resource
   *
   * @param {Object} variant
   */
  setVariant(variant: string) {
    if (variant) {
      this.variant = variant;
    }

    return this;
  }

  /**
   * Returns the transformer variant
   */
  getVariant() {
    return this.variant;
  }

  setResourceKey(resourceKey: string) {
    this.resourceKey = resourceKey;

    return this;
  }

  getResourceKey() {
    return this.resourceKey;
  }

  /**
   * When a transformer string is passed with a variation defined in dot-notation
   * we will split the string into transformer and variant
   */
  _separateTransformerAndVariation(transformerString: any) {
    // This feature is only available when a string binding is used
    if (typeof transformerString !== 'string') {
      return { transformer: transformerString, variant: null };
    }

    const regex = /(.*)\.(.*)/;

    const matches = transformerString.match(regex);

    // if the string did not contain a variation use the
    // transformerString is used and the variation is set to null
    const transformer = matches ? matches[1] : transformerString;
    const variant = matches ? matches[2] : null;

    return { transformer, variant }
  }
}

export default ResourceAbstract;
