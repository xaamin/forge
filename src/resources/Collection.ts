import ResourceAbstract from './ResourceAbstract';

/**
 * Collection class
 *
 * @class Collection
 * @extends ResourceAbstract
 */
class Collection extends ResourceAbstract {
}

export default Collection;
