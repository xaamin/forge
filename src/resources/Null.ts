import ResourceAbstract from './ResourceAbstract';

/**
 * Null class
 *
 * @class Null
 */
class Null extends ResourceAbstract {
  /**
   * Overwrite the constructor and set data and transformer to null
   */
  constructor () {
    super(null, null, null);
  }

  /**
   * Returns null, a NullResource always returns null
   */
  async getData () {
    return null
  }
}

export default Null;
