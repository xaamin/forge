import ResourceAbstract from './ResourceAbstract';

/**
 * Item class
 *
 * @class Item
 */
class Item extends ResourceAbstract {
}

export default Item;
