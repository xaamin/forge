import { camelCase as _camelCase } from 'lodash';

import Scope from './Scope';
import { ResourceAbstract, Collection, Item, Null } from './resources';

/**
 * TransformerAbstract class
 *
 * @class TransformerAbstract
 * @constructor
 */
class TransformerAbstract<T extends any = {}, U extends any = {}> {
  /*
   * Resources that can be included if requested
  */
  availableIncludes: string[] = [];

  /*
   * List of resources to automatically include
  */
  defaultIncludes: string[] = [];

  /**
   * This method is used to transform the data.
   * Implementation required
   */
  transform(data: T | T[]): U {
    throw new Error('You have to implement the method transform or specify a variant when calling the transformer!');
  }

  /**
   * Helper method to transform a collection in includes
   *
   * @param {*} data
   * @param {*} transformer
   */
  collection(data: unknown[], transformer: TransformerAbstract, resourceKey: null | string = null): Collection {
    return new Collection(data, transformer, resourceKey);
  }

  /**
   * Helper method to transform an object in includes
   *
   * @param {*} data
   * @param {*} transformer
   */
  item(data: unknown, transformer: TransformerAbstract, resourceKey: null | string = null) {
    return new Item(data, transformer, resourceKey);
  }

  /**
   * Helper method to return a null resource
   *
   * @param {*} data
   * @param {*} transformer
   */
  null() {
    return new Null();
  }

  /**
   * Processes included resources for this transformer
   *
   * @param {*} parentScope
   * @param {*} data
   */
  async _processIncludedResources(parentScope: Scope, data: any) {
    const includeData: any = {};

    // figure out which of the available includes are requested
    const resourcesToInclude = this._figureOutWhichIncludes(parentScope);

    // for each include call the include function for the transformer
    for (const include of resourcesToInclude) {
      const resource = await this._callIncludeFunction(include, parentScope, data);

      // if the include uses a resource, run the data through the transformer chain
      if (resource instanceof ResourceAbstract) {
        includeData[include] = await this._createChildScopeFor(parentScope, resource, include).toJSON();
      } else {
        // otherwise, return the data as is
        includeData[include] = resource;
      }
    }

    return includeData;
  }

  /**
   * Construct and call the include function
   *
   * @param {*} include
   * @param {*} parentScope
   * @param {*} data
   */
  async _callIncludeFunction(include: string, parentScope: Scope, data: any) {
    // convert the include name to camelCase
    include = _camelCase(include)
    const includeName = `include${include.charAt(0).toUpperCase()}${include.slice(1)}`;

    if (!(this[includeName] instanceof Function)) {
      throw new Error(`A method called '${includeName}' could not be found in '${this.constructor.name}'`);
    }

    return this[includeName](data);
  }

  /**
   * Returns an array of all includes that are requested
   *
   * @param {*} parentScope
   */
  _figureOutWhichIncludes(parentScope: Scope) {
    const includes = this.defaultIncludes;

    const requestedAvailableIncludes = this.availableIncludes.filter(i => parentScope._isRequested(i));

    return includes.concat(requestedAvailableIncludes);
  }

  /**
   * Create a new scope for the included resource
   *
   * @param {*} parentScope
   * @param {*} resource
   * @param {*} include
   */
  _createChildScopeFor(parentScope: Scope, resource: ResourceAbstract, include: string) {
    // create a new scope
    const Scope = require('./Scope').default;

    const childScope = new Scope(parentScope._manager, resource, include);

    // get the scope for this transformer
    const scopeArray = [...parentScope.getParentScopes()];

    if (parentScope.getScopeIdentifier()) {
      const identifier = parentScope.getScopeIdentifier();

      if (identifier) {
        scopeArray.push(identifier);
      }
    }

    // set the parent scope for the new child scope
    childScope.setParentScopes(scopeArray);

    return childScope
  }
}

export default TransformerAbstract;
