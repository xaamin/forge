import SerializerAbstract from './SerializerAbstract';

/**
 * PlainSerializer class
 *
 * @class PlainSerializer
 * @constructor
 */
class PlainSerializer extends SerializerAbstract {
  /**
   * Serialize a collection of data
   * The PlainSerializer will just return the data without modification
   *
   * @param {Array} data
   */
  async collection(data: any[], resourceKey: null | string, depth = 0): Promise<unknown[]> {
    return data;
  }

  /**
   * Serialize a single item
   * The PlainSerializer will return the the data without modification
   *
   * @param {*} data
   */
  async item(data: any, resourceKey: null | string, depth = 0): Promise<any> {
    return data;
  }
}

export default PlainSerializer;
