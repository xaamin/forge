import SerializerAbstract from './SerializerAbstract';

/**
 * DataSerializer class
 *
 * @class DataSerializer
 * @constructor
 */
class SldDataSerializer extends SerializerAbstract {
  /**
   * Serialize a collection of data
   * The DataSerializer will add all data under the 'data' namespace
   *
   * @param {Array} data
   */
  async collection(data: any[], resourceKey: null | string, depth: number = 0) {
    if (depth === 0) {
      return { data: data };
    }

    return data;
  }

  /**
   * Serialize a single item
   * The DataSerializer will add the item under the 'data' namespace
   *
   * @param {*} data
   */
  async item(data: any, resourceKey: null | string, depth = 0) {
    // if the item is an object, add it to the data property
    if (depth === 0 && data instanceof Object) {
      return { data: data };
    }

    // If the data for this item is not a object, aka. a primitive type
    // we will just return the plain data.
    return data;
  }
}

export default SldDataSerializer;
