import { ResourceAbstract } from '../resources';
import SerializerAbstract from './SerializerAbstract';

class JsonApiSerializer extends SerializerAbstract {
  baseUrl = null;
  rootObjects: string[] = [];

  constructor(baseUrl = null) {
    super();

    this.baseUrl = baseUrl;
  }

  async collection(data: any[], resourceKey: string | null = null, depth = 0) {
    const resources = [];

    for (const resource of data) {
      const item = await this.item(resource, resourceKey, depth);

      resources.push(item['data']);
    }

    return { 'data': resources };
  }

  async item(data: any, resourceKey: string | null = null, depth = 0) {
    const id = this.getIdFromData(data);
    let custom_links = null;

    const resource: any = {
      'data': {
        'type': resourceKey,
        'id': id,
        'attributes': data,
      },
    };

    delete resource['data']['attributes']['id'];

    if (!resource['data']['attributes']['links']) {
      custom_links = data['links'];

      delete resource['data']['attributes']['links'];
    }

    if (!resource['data']['attributes']['meta']) {
      resource['data']['meta'] = data['meta'];

      delete resource['data']['attributes']['meta'];
    }

    if (!resource['data']['attributes']) {
      resource['data']['attributes'] = {};
    }

    if (this.shouldIncludeLinks()) {
      resource['data']['links'] = {
        'self': `${this.baseUrl}/${resourceKey}/${id}`,
      };

      if (custom_links) {
        resource['data']['links'] = [].concat(resource['data']['links'], custom_links);
      }
    }

    return resource;
  }

  paginator(paginator: any) {
    const currentPage = paginator.getCurrentPage();
    const lastPage = paginator.getLastPage();

    const pagination: any = {
      'total': paginator.getTotal(),
      'count': paginator.getCount(),
      'per_page': paginator.getPerPage(),
      'current_page': currentPage,
      'total_pages': lastPage,
    };

    pagination['links'] = [];

    pagination['links']['self'] = paginator.getUrl(currentPage);
    pagination['links']['first'] = paginator.getUrl(1);

    if (currentPage > 1) {
      pagination['links']['prev'] = paginator.getUrl(currentPage - 1);
    }

    if (currentPage < lastPage) {
      pagination['links']['next'] = paginator.getUrl(currentPage + 1);
    }

    pagination['links']['last'] = paginator.getUrl(lastPage);

    return { 'pagination': pagination };
  }

  async meta(meta: any) {
    if (!meta || meta.length === 0) {
      return [];
    }

    const result: any = {
      'meta': meta
    };

    if (result['meta'] && result['meta']['pagination']) {
      result['links'] = result['meta']['pagination']['links'];

      delete result['meta']['pagination']['links'];
    }

    return result;
  }

  null() {
    return {
      'data': null,
    };
  }

  includeData(data: any[]) {
    let [serializedData, linkedIds] = this.pullOutNestedIncludedData(data);

    for (const value of data) {
      for (const includeObject of Object.values(value)) {
        if (this.isPrimitive(includeObject)) {
          continue;
        }

        const includeObjects = this.createIncludeObjects(includeObject);

        [serializedData, linkedIds] = this.serializeIncludedObjectsWithCacheKey(
          includeObjects,
          linkedIds,
          serializedData
        );
      }
    }

    return !serializedData || serializedData.length === 0 ? {} : { included: serializedData };
  }

  sideloadIncludes() {
    return true;
  }

  injectData(data: unknown, rawIncludedData: unknown[]) {
    if (this.isPrimitive(data) || this.isNull(data as any) || this.isEmpty(data as any)) {
      return data;
    }

    const relationships = this.parseRelationships(rawIncludedData);

    if (!relationships || relationships.length === 0) {
      data = this.fillRelationships(data, relationships);
    }

    return data;
  }

  /**
  *
  * Hook to manipulate the final sideloaded includes.
  * The JSON API specification does not allow the root object to be included
  * into the sideloaded `included`-array. We have to make sure it is
  * filtered out, in case some object links to the root object in a
  * relationship.
  */
  filterIncludes(includedData: any, data: any) {
    if (!includedData['included']) {
      return includedData;
    }

    // Create the RootObjects
    this.createRootObjects(data);

    // Filter out the root objects
    const filteredIncludes = includedData['included'].filter(this.filterRootObject.bind(this));

    // Reset array indizes
    includedData['included'] = [].concat(filteredIncludes);

    return includedData;
  }

  getMandatoryFields() {
    return ['id'];
  }

  /**
  * Filter function to delete root objects from array.
  */
  filterRootObject(object: any) {
    return !this.isRootObject(object);
  }

  /**
  * Set the root objects of the JSON API tree.
  */
  setRootObjects(objects: any[] = []) {
    this.rootObjects = objects.map((object) => {
      return `${object['type']}:${object['id']}`;
    });
  }

  /**
  * Determines whether an object is a root object of the JSON API tree.
  */
  isRootObject(object: any) {
    const objectKey = `${object['type']}:${object['id']}`;

    return this.rootObjects.includes(objectKey);
  }

  isCollection(data: { data?: any}) {
    return data['data'] && !!data['data'][0];
  }

  isNull(data: { data?: any}) {
    return data['data'] && data['data'][0] === null;
  }

  isEmpty(data: { data?: any[]}) {
    return data['data'] && data['data'][0] && Array.isArray(data['data'][0]) && data['data'][0].length === 0;
  }

  isPrimitive(data: any) {
    return data !== undefined && data !== null && typeof data !== 'object' && typeof data !== 'function' && !Array.isArray(data);
  }

  fillRelationships(data: any, relationships: any) {
    if (this.isCollection(data)) {
      for (const index in relationships) {
        const relationship = relationships[index];

        data = this.fillRelationshipAsCollection(data, relationship, index);
      }
    } else { // Single resource
       for (const index in relationships) {
        const relationship = relationships[index];

        data = this.fillRelationshipAsSingleResource(data, relationship, index);
      }
    }

    return data;
  }

  parseRelationships(includedData: any[]) {
    let relationships = [];

    for (const index in includedData) {
      const inclusion = includedData[index];

      for (const includeKey in inclusion) {
        const includeObject = inclusion[includeKey];

        if (this.isNull(includeObject) || this.isEmpty(includeObject)) {
          continue;
        }

        relationships = this.buildRelationships(includeKey, relationships, includeObject, index);

        if (includedData[0][includeKey]['meta']) {
          relationships[includeKey][0]['meta'] = includedData[0][includeKey]['meta'];
        }
      }
    }

    return relationships;
  }

  getIdFromData(data: { id?: string | number}) {
    if (!data['id']) {
      throw new Error(
        'JSON API resource objects MUST have a valid id'
      );
    }

    return data['id'];
  }

  /**
  * Keep all sideloaded inclusion data on the top level.
  */
  pullOutNestedIncludedData(data: any) {
    let includedData = [];
    let linkedIds = [];

    for (const value of data) {
      const values: any[] = Object.values(value);

      for (const includeObject of values) {
        if (includeObject['included']) {
          [includedData, linkedIds] = this.serializeIncludedObjectsWithCacheKey(
            includeObject['included'],
            linkedIds,
            includedData
          );
        }
      }
    }

    return [includedData, linkedIds];
  }

  /**
  * Whether or not the serializer should include `links` for resource objects.
  */
  shouldIncludeLinks() {
    return this.baseUrl !== null;
  }

  /**
  * Check if the objects are part of a collection or not
  *
  * @param array|object includeObject
  */
  createIncludeObjects(includeObject: any) {
    if (this.isCollection(includeObject)) {
      return includeObject['data'];
    } else {
      return [includeObject['data']];
    }
  }

  /**
  * Sets the RootObjects, either as collection or not.
  */
  createRootObjects(data: { data: any }) {
    if (this.isCollection(data)) {
      this.setRootObjects(data['data']);
    } else {
      this.setRootObjects([data['data']]);
    }
  }

  /**
  * Loops over the relationships of the provided data and formats it
  */
  fillRelationshipAsCollection(data: any, relationship: { [k: string]: any[] }, key: string) {
    for (const index in relationship) {
      const relationshipData = relationship[index];

      if (!data['data'][index]['relationships']) {
        data['data'][index]['relationships'] = {};
      }

      data['data'][index]['relationships'][key] = relationshipData;
    }

    return data;
  }

  fillRelationshipAsSingleResource(data: any, relationship: any[], key: string) {
    if (!data['data']['relationships']) {
      data['data']['relationships'] = {};
    }

    data['data']['relationships'][key] = relationship[0];

    return data;
  }

  buildRelationships(includeKey: string, relationships: any, includeObject: any, key: string) {
    let relationship = null;

    relationships = this.addIncludekeyToRelationsIfNotSet(includeKey, relationships);

    if (this.isNull(includeObject)) {
      relationship = this.null();
    } else if (this.isEmpty(includeObject)) {
      relationship = {
        'data': [],
      };
    } else if (this.isPrimitive(includeObject)) {
      relationship = {
        'data': {
          type: includeKey,
          value: includeObject
        },
      };
    } else if (this.isCollection(includeObject)) {
      relationship = { 'data': [] };

      relationship = this.addIncludedDataToRelationship(includeObject, relationship);
    } else {
      relationship = {
        'data': {
          'type': includeObject['data']['type'],
          'id': includeObject['data']['id'],
        },
      };
    }

    relationships[includeKey][key] = relationship;

    return relationships;
  }

  addIncludekeyToRelationsIfNotSet(includeKey: string, relationships: any) {
    if (!relationships[includeKey]) {
      relationships[includeKey] = [];

      return relationships;
    }

    return relationships;
  }

  addIncludedDataToRelationship(includeObject: { data: any[] }, relationship: any) {
    for (const obj of includeObject['data']) {
      relationship['data'].push({
        'type': obj['type'],
        'id': obj['id'],
      });
    }

    return relationship;
  }

  injectAvailableIncludeData(data: any, availableIncludes: any[]) {
    if (!this.shouldIncludeLinks()) {
      return data;
    }

    if (this.isCollection(data)) {
      data['data'] = data['data'].map((resource: any) => {
        for (const relationshipKey of availableIncludes) {
          resource = this.addRelationshipLinks(resource, relationshipKey);
        }

        return resource;
      });
    } else {
      for (const relationshipKey of availableIncludes) {
        data['data'] = this.addRelationshipLinks(data['data'], relationshipKey);
      }
    }

    return data;
  }

  /**
  * Adds links for all available includes to a single resource.
  *
  * @param resource         The resource to add relationship links to
  * @param relationshipKey The resource key of the relationship
  */
  addRelationshipLinks(resource: any, relationshipKey: string) {
    if (!resource['relationships']) {
      resource['relationships'] = {};
    }

    if (!resource['relationships'][relationshipKey]) {
      resource['relationships'][relationshipKey] = {};
    }

    const links = {
      'links': {
        'self': `${this.baseUrl}/${resource['type']}/${resource['id']}/relationships/${relationshipKey}`,
        'related': `${this.baseUrl}/${resource['type']}/${resource['id']}/${relationshipKey}`,
      }
    };

    resource['relationships'][relationshipKey] = Object.assign(
      {}, links, resource['relationships'][relationshipKey]
    );

    return resource;
  }

  serializeIncludedObjectsWithCacheKey(includeObjects: any[], linkedIds: any, serializedData: any) {
      for (const obj of includeObjects) {
        const includeType = obj['type'];
        const includeId = obj['id'];
        const cacheKey = `${includeType}:${includeId}`;

        if (!linkedIds[cacheKey]) {
          serializedData.push(obj);

          linkedIds[cacheKey] = obj;
        }
      }

      return [serializedData, linkedIds];
    }
  }

  export default JsonApiSerializer;
