import { ResourceAbstract } from "../resources";

/**
 * SerializerAbstract class
 *
 * @class SerializerAbstract
 */
class SerializerAbstract {
  /**
   * Serialize a collection of data
   * You must implement this method in your Serializer
   *
   * @param {Array} data
   */
  async collection(data: unknown[], resourceKey: null | string, depth = 0): Promise<unknown> {
    throw new Error('A Serializer must implement the method collection')
  }

  /**
   * Serialize a single item of data
   * You must implement this method in your Serializer
   *
   * @param {*} data
   */
  async item(data: any, resourceKey: null | string = null, depth = 0): Promise<unknown> {
    throw new Error('A Serializer must implement the method item')
  }

  /**
   * Serialize a null value
   */
  null(): null | any {
    return null;
  }

  /**
   * Serialize a meta object
   *
   * @param {Object} meta
   */
  async meta(meta: { [k: string]: unknown }) {
    return { meta: meta }
  }

  /**
   * Serialize the pagination meta data
   *
   * @param {Object} pagination
   */
  paginator(pagination: { [k: string]: any }) {
    return { pagination: pagination }
  }

  /**
   * Merge included data with the main data for the resource.
   * Both includes and data have passed through either the
   * 'item' or 'collection' method of this serializer.
   *
   * @param {Object} data
   * @param {Object} includes
   */
  mergeIncludes(data: any, includes: any): any {
    // Include the includes data first.
    // If there is data with the same key as an include, data will take precedence.
    if (!this.sideloadIncludes()) {
      return Object.assign(includes, data);
    }

    return data;
  }

  sideloadIncludes() {
    return false;
  }

  includeData(data: unknown[]):  unknown {
    return data;
  }

  injectData(data: unknown[], rawIncludedData: unknown[]): unknown {
    return data;
  }

  filterIncludes(includedData: unknown[], data: unknown[]): unknown[] {
    return includedData;
  }

  injectAvailableIncludeData(data: unknown[], includes: string[]): any {
    return data;
  }
}

export default SerializerAbstract;
