import Manager from './Manager';
import * as Resources from './resources';
import { SerializerAbstract } from './serializers';
import TransformerAbstract from './TransformerAbstract';

/**
 * Forge class
 *
 * @class Forge
 * @constructor
 */
class Forge {
  _data: any | any[] | null = null;
  _dataType: string | null = null;
  _transformer: TransformerAbstract | null = null;
  _variant: string | null = null;
  _resourceKey: string | null = null;

  _pagination: any = null;
  _meta: { [k: string]: unknown } | null = null;

  _manager: Manager;

  /**
   * Create a new Forge instance.
   * An instance of Manager has to be passed
   *
   * @param {Manager} manager
   */
  constructor(manager: Manager) {
    this._manager = manager;
  }

  /**
   * Creates a new instance of Forge
   * Data and transformer can optionally be passed or set via setters in the instance.
   *
   * @param {*} data
   * @param {TransformerAbstract} transformer
   */
  static make(data: any = null, transformer: TransformerAbstract | null = null, resourceKey: string | null) {
    // create an instance of Forge and pass a new instance of Manager
    const instance = new Forge(new Manager());

    // initialize data and transformer properties
    const dataType = instance._determineDataType(data);

    instance._setData(dataType, data, resourceKey)

    if (transformer) {
      instance.withTransformer(transformer);
    }

    // return the instance for the fluid interface
    return instance;
  }

  /**
   * Add a collection of data to be transformed.
   * If a transformer is passed, the fluid interface is terminated
   *
   * @param {Array} data
   * @param {TransformerAbstract} transformer
   * @param {String} resourceKey
   */
  collection(data: any[], transformer: TransformerAbstract | null = null, resourceKey: string | null = null) {
    this._setData('Collection', data, resourceKey);

    if (transformer) {
      this.withTransformer(transformer);
    }

    return this;
  }

  /**
   * Add data that should be transformed as a single item.
   * If a transformer is passed, the fluid interface is terminated
   *
   * @param {Array} data
   * @param {TransformerAbstract} transformer
   * @param {String} resourceKey
   */
  item(data: any, transformer: TransformerAbstract | null = null, resourceKey: string | null = null) {
    this._setData('Item', data, resourceKey);

    if (transformer) {
      this.withTransformer(transformer);
    }

    return this;
  }

  /**
   * Sets data to null
   */
  null() {
    this._setData('Null', null);

    return this;
  }

  /**
   * Add a collection of data to be transformed.
   * Works just like collection but requires data to be a lucid paginated model.
   * If a transformer is passed, the fluid interface is terminated
   *
   * @param {Array} data
   * @param {TransformerAbstract} transformer
   * @param {String} resourceKey
   */
  paginate(data: any, transformer = null, resourceKey: string | null = null) {
    this._setData('Collection', data.rows);

    // extract pagination data
    const paginationData = data.pages

    // ensure the pagination keys are integers
    Object.keys(paginationData).forEach((key) => {
      paginationData[key] = parseInt(paginationData[key]);
    })

    // set pagination data
    this._pagination = paginationData;

    if (transformer) {
      this.withTransformer(transformer);
    }

    return this;
  }

  /**
   * Add additional meta data to the object under transformation
   *
   * @param {Object} meta
   */
  meta(meta: { [k: string]: unknown }) {
    return this.withMeta(meta);
  }

  /**
   * Add additional meta data to the object under transformation
   *
   * @param {Object} meta
   */
  withMeta(meta: { [k: string]: unknown }) {
    this._meta = meta;

    return this;
  }

  /**
   * Set the transformer
   *
   * @param {TransformerAbstract} transformer
   */
  transformer(transformer: TransformerAbstract) {
    return this.withTransformer(transformer);
  }

  /**
   * Set the transformer
   *
   * @param {TransformerAbstract} transformer
   */
  withTransformer(transformer: TransformerAbstract) {
    this._transformer = transformer;

    return this;
  }

  /**
   * Set the transformer variant
   *
   * @param {String} variant
   */
  variant(variant: string) {
    return this.withVariant(variant);
  }

  /**
   * Set the transformer variant
   *
   * @param {String} variant
   */
  withVariant(variant: string) {
    this._variant = variant;

    return this;
  }

  /**
   * Additional resources that should be included and that are defined as
   * 'availableIncludes' on the transformer.
   *
   * @param {Array|String} include
   */
  include(include: string | string[]) {
    this._manager.parseIncludes(include);

    return this;
  }
  /**
   * Additional resources that should be included and that are defined as
   * 'availableIncludes' on the transformer.
   *
   * @param {Array, String} include
   */
  including(include: string | string[]) {
    return this.include(include);
  }

  /**
   * Set the serializer for this transformation.
   *
   * @param {SerializerAbstrace} serializer
   */
  serializer(serializer: SerializerAbstract) {
    return this.withSerializer(serializer)
  }

  /**
   * Alias for 'setSerializer'
   *
   * @param {SerializerAbstrace} serializer
   */
  withSerializer(serializer: SerializerAbstract) {
    this._manager.setSerializer(serializer);

    return this;
  }

  getManager() {
    return this._manager;
  }

  /**
   * Terminates the fluid interface and returns the transformed data.
   */
  toJSON() {
    return this._createData().toJSON();
  }

  /**
   * @param {String} dataType
   * @param {Object} data
   * @param {String} resourceKey
   */
  _setData(dataType: string, data: any, resourceKey: string | null = null) {
    this._data = data;
    this._dataType = dataType;
    this._pagination = null;
    this._resourceKey = resourceKey;

    return this
  }

  /**
   * Helper function to set resource on the manager
   */
  _createData() {
    return this._manager.createData(this._getResource());
  }

  /**
   * Create a resource for the data and set meta and pagination data
   */
  _getResource() {
    const Resource = (Resources as  any)[String(this._dataType)];
    const resourceInstance = new Resource(this._data, this._transformer);

    resourceInstance.setMeta(this._meta);
    resourceInstance.setPagination(this._pagination);
    resourceInstance.setVariant(this._variant);
    resourceInstance.setResourceKey(this._resourceKey);

    return resourceInstance;
  }

  /**
   * Determine resource type based on the type of the data passed
   *
   * @param {*} data
   */
  _determineDataType(data: any) {
    if (data === null) {
      return 'Null';
    }

    if (Array.isArray(data)) {
      return 'Collection';
    }

    return 'Item';
  }
}

export default Forge;
