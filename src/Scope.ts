import Manager from './Manager';
import TransformerAbstract from './TransformerAbstract';
import { Collection, Item, Null, ResourceAbstract } from './resources';
import { SerializerAbstract } from './serializers';

/**
 * Scope class
 *
 * @class Scope
 * @constructor
 */
class Scope {
  _manager;
  _resource;
  _scopeIdentifier: string | null;
  _parentScopes: string[];
  _availableIncludes: string[];

  /**
   * Create a new Scope class
   *
   * @param {*} manager
   * @param {*} resource
   * @param {*} scopeIdentifier
   */
  constructor(manager: Manager, resource: ResourceAbstract, scopeIdentifier = null) {
    this._manager = manager;
    this._resource = resource;
    this._scopeIdentifier = scopeIdentifier;
    this._parentScopes = [];
    this._availableIncludes = [];
  }

  /**
   * Passes the data through the transformers and serializers and returns the transformed data
   */
  async toJSON() {
    // run the transformation on the data
    const [rawData, rawIncludedData] = await this._executeResourceTransformers();

    // create a serializer instance
    const serializer = this._manager.getSerializer();

    // run the raw data through the serializer
    let data = await this._serializeResource(serializer, rawData);

    if (serializer.sideloadIncludes() && rawIncludedData && rawIncludedData.length > 0) {
      //Filter out any relation that wasn't requested
      let includedData = serializer.includeData(rawIncludedData);

      // If the serializer wants to inject additional information
      // about the included resources, it can do so now.
      data = serializer.injectData(data, rawIncludedData);

      if (this.isRootScope()) {
          // If the serializer wants to have a final word about all
          // the objects that are sideloaded, it can do so now.
          includedData = serializer.filterIncludes(
              includedData,
              data
          );
      }

      data = Object.assign({}, data, includedData);
    }

    if (this._availableIncludes.length > 0) {
      data = serializer.injectAvailableIncludeData(data, this._availableIncludes);
    }

    // initialize an empty meta object
    let meta = {};

    // if the resource is a collection and there is pagination data...
    if (this._resource instanceof Collection && this._resource.getPagination()) {
      // run the pagination data through the serializer and add it to the meta object
      const pagination = serializer.paginator(this._resource.getPagination());
      meta = Object.assign({}, pagination, meta);
    }

    // if there is custom meta data, add it the our meta object
    if (this._resource.getMeta()) {
      meta = await serializer.meta(this._resource.getMeta());
    }

    // If any meta data has been added, add it to the response
    if (Object.keys(meta).length !== 0) {
      // If the serializer does not support meta data,
      // we just force the data object under a 'data' propert since we can not mix an array with objects
      if (Array.isArray(data) || (typeof data !== 'object' && data !== null)) {
        data = { data: data };
      }

      // merge data with meta data
      data = Object.assign({}, meta, data);
    }

    // all done, return the transformed data
    return data;
  }

  /**
   * Creates a transformer instance and runs data through the transformer
   */
  async _executeResourceTransformers() {
    // get a transformer and fetch data from the resource
    const transformer = this._resource.getTransformer()!;
    const data = await this._resource.getData();

    let transformedData: any | null = [];
    const includedData: any[] = [];

    if (!data || this._resource instanceof Null) {
      // If the resource is a null-resource, set data to null without includes
      transformedData = null;
    } else if (this._resource instanceof Item) {
      // It the resource is an item, run the data through the transformer
      const [transformedValue, includedValue] = await this._fireTransformer(data, transformer);

      transformedData = transformedValue;

      if (includedValue) {
        includedData.push(includedValue);
      }
    } else if (this._resource instanceof Collection) {
      // It we have a collection, get each item from the array of data
      // and run each item individually through the transformer
      for (const value of data) {
        const [transformedValue, includedValue] = await this._fireTransformer(value, transformer);

        transformedData.push(transformedValue);

        if (includedValue) {
          includedData.push(includedValue);
        }
      }
    } else {
      // If we are here, we have some unknown resource and can not transform it
      throw new Error('This resourcetype is not supported. Use Item or Collection');
    }

    return [transformedData, includedData];
  }

  /**
   * Runs an object of data through a transformer method
   *
   * @param {*} data
   * @param {*} transformer
   */
  async _fireTransformer(data: any, transformer: TransformerAbstract) {
    let includedData: any | null = null;

    // get a transformer instance and tranform data
    const transformerInstance = this._getTransformerInstance(transformer);
    let transformedData = await this._dispatchToTransformerVariant(transformerInstance, data);

    // if this transformer has includes defined,
    // figure out which includes should be run and run requested includes
    if (this._transformerHasIncludes(transformerInstance)) {
      includedData = await transformerInstance._processIncludedResources(this, data);
      transformedData = await this._manager.getSerializer().mergeIncludes(transformedData, includedData);

      this._availableIncludes = [...transformerInstance.defaultIncludes, ...transformerInstance.availableIncludes];
    }

    return [transformedData, includedData];
  }

  /**
   * Run data through a serializer
   *
   * @param {*} serializer
   * @param {*} rawData
   */
  async _serializeResource(serializer: SerializerAbstract, rawData: any | unknown[]) {
    const scopeDepth = this.getScopeArray().length;

    if (this._resource instanceof Collection) {
      return serializer.collection(rawData, this._resource.getResourceKey(), scopeDepth);
    }

    if (this._resource instanceof Item) {
      return serializer.item(rawData, this._resource.getResourceKey(), scopeDepth);
    }

    return serializer.null();
  }

  /**
   * Checks if this scope is requested by comparing the current nesting level with the requested includes
   *
   * @param {*} checkScopeSegment
   */
  _isRequested(checkScopeSegment: string) {
    // create the include string by combining current level with parent levels
    const scopeString = [...this.getScopeArray(), checkScopeSegment].join('.');

    // check if this include was requested. If the include does not occur in the
    // requested includes, we check again, for it may have been requested using
    // snake_case instead of camelCase
    return this._manager.getRequestedIncludes().includes(scopeString) ||
      this._manager.getRequestedIncludes(true).includes(scopeString);
  }

  /**
   * Creates and returns a new transformer instance
   *
   * @param {*} Transformer
   */
  _getTransformerInstance(Transformer: TransformerAbstract | Function): TransformerAbstract | never {

    // if the transformer is a class, create a new instance
    // if (Transformer && Transformer.prototype && Transformer.prototype instanceof TransformerAbstract) {
    //   return new Transformer();
    // }

    if (Transformer instanceof TransformerAbstract) {
      return Transformer;
    }

    if (typeof Transformer === 'function') {
      // if a closure was passed, we create an anonymous transformer class
      // with the passed closure as transform method
      class ClosureTransformer extends TransformerAbstract {
        transform (data: any | unknown[]) {
          return (Transformer as CallableFunction)(data);
        }
      }

      return new ClosureTransformer();
    }

    throw new Error('A transformer must be a function or a class extending TransformerAbstract');
  }

  /**
   * Checks if any variants are defined and calls the corresponding transform method
   *
   * @param {*} transformerInstance
   * @param {*} data
   */
  async _dispatchToTransformerVariant(transformerInstance: TransformerAbstract, data: any | unknown[]) {
    const variant: string | null = this._resource.getVariant();

    //  if a variant was defined, we construct the name for the transform mehod
    // otherwise, the default transformer method 'transform' is called
    const transformMethodName = variant
      ? `transform${variant.charAt(0).toUpperCase()}${variant.slice(1)}`
      : 'transform';

    // Since the user can pass anything as a variant name, we need to
    // validate that the transformer method exists.
    if (!(transformMethodName in transformerInstance && transformerInstance[transformMethodName] instanceof Function)) {
      throw new Error(`A transformer method '${transformMethodName}' could not be found in '${transformerInstance.constructor.name}'`);
    }

    // now we call the transformer method on the transformer and return the data
    return (transformerInstance as any)[transformMethodName](data);
  }

  /**
   * Check if the used transformer has any includes defined
   *
   * @param {*} Transformer
   */
  _transformerHasIncludes(Transformer: TransformerAbstract) {
    const defaultIncludes = Transformer.defaultIncludes;
    const availableIncludes = Transformer.availableIncludes;

    return defaultIncludes.length > 0 || availableIncludes.length > 0
  }

  /**
   * Set the parent scope identifier
   */
  setParentScopes(parentScopes: any[]) {
    this._parentScopes = parentScopes;
  }

  /**
   *  Returns the parents scope identifier
   */
  getParentScopes() {
    return this._parentScopes;
  }

  /**
   * Get the identifier for this scope
   */
  getScopeIdentifier() {
    return this._scopeIdentifier;
  }

  getScopeArray() {
    if (this._scopeIdentifier) {
      return [...this._parentScopes, this._scopeIdentifier];
    }

    return [];
  }

  /**
     * Check, if this is the root scope.
     */
   isRootScope(){
    return this.getParentScopes().length === 0;
   }
}

export default Scope;
