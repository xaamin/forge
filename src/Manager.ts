import { camelCase as _camelCase } from 'lodash';
import Scope from './Scope';
import { SerializerAbstract, DataSerializer, PlainSerializer, SldDataSerializer, JsonApiSerializer } from './serializers';

/**
 * Manager class
 *
 * @class Manager
 * @constructor
 */
class Manager {
  serializer: SerializerAbstract | null;
  requestedIncludes: string[];
  _recursionLimit = 10;

  /**
   * Create a new manager instance
   */
  constructor() {
    this.serializer = null;
    this.requestedIncludes = [];
    this._recursionLimit = 10;
  }

  /**
   * Create a Scope instance for the resource
   *
   * @param {*} resource
   */
  createData(resource: any) {
    this._setIncludesFromRequest();

    return new Scope(this, resource);
  }

  /**
   * Returns the requested includes. An optional parameter converts snake_case
   * includes to standardized camelCase
   */
  getRequestedIncludes(transformCamelCase = false) {
    if (!transformCamelCase) return this.requestedIncludes;

    const includes = [...this.requestedIncludes]
      .map(i => i.split('.').map(_camelCase).join('.'));

    return includes;
  }

  /**
   * Parses an include string or array and constructs an array of all requested includes
   *
   * @param {*} includes
   */
  parseIncludes(includes: string | string[]) {
    this.requestedIncludes = [];

    // if a string is passed, split by comma and return an array
    if (typeof includes === 'string') {
      includes = includes.split(',').map((value) => value.trim());
    }

    // if it is not an array, we can not parse it at this point
    if (!Array.isArray(includes)) {
      throw Error(`The parseIncludes() method expects a string or an array. ${typeof includes} given`);
    }

    // sanitize the includes
    includes = includes.map(i => this._guardAgainstToDeepRecursion(i));

    // add all includes to the internal set
    includes.forEach((value) => {
      if (!this.requestedIncludes.includes(value)) {
        this.requestedIncludes.push(value);
      }
    });

    this._autoIncludeParents();
  }

  /**
   * Allowes setting a custom recursion limit
   *
   * @param {*} limit
   */
  setRecursionLimit(limit: number) {
    this._recursionLimit = limit;

    return this;
  }

  /**
   * Create a serializer
   *
   * @param {*} serializer
   */
  setSerializer(serializer: SerializerAbstract | string) {
    if (typeof serializer === 'string') {
      switch (serializer) {
        case 'data':
          serializer = new DataSerializer();
          break;

        case 'sld':
          serializer = new SldDataSerializer();
          break;

        case 'plain':
          serializer = new PlainSerializer();
          break;

        case 'json-api':
          serializer = new JsonApiSerializer() as SerializerAbstract;
          break;

        default:
          throw new Error(`No data serializer for ${serializer}`);
      }
    }

    this.serializer = serializer;
  }

  /**
   * Get an instance if the serializer, if not set, use setting from the config
   */
  getSerializer(): SerializerAbstract {
    if (!this.serializer) {
      this.setSerializer(new PlainSerializer);
    }

    return this.serializer as SerializerAbstract;
  }

  /**
   * To prevent to many recursion, we limit the number of nested includes allowed
   *
   * @param {*} include
   */
  _guardAgainstToDeepRecursion(include: string) {
    return include.split('.').slice(0, this._recursionLimit).join('.');
  }

  /**
   * Add all the resources along the way to a nested include
   */
  _autoIncludeParents() {
    const parsed = [];

    // for each resource that is requested
    for (const include of this.requestedIncludes) {
      // we split it by '.' to get the recursions
      const nested = include.split('.');

      // Add the first level to the includes
      let part = nested.shift() as string;
      parsed.push(part);

      // if there are more nesting levels,
      // add each level to the includes
      while (nested.length) {
        part += `.${nested.shift()}`;
        parsed.push(part);
      }
    }

    // add all parsed includes to the set of requested includes
    parsed.forEach((value) => {
      if (!this.requestedIncludes.includes(value)) {
        this.requestedIncludes.push(value);
      }
    });
  }

  /**
   * Parses the request object from the context and extracts the requested includes
   *
   * @param {*} ctx
   */
  _setIncludesFromRequest() {
    // get all get parameters from the request
    // if the 'include' parameter is set, pass it the the parse method
    const params: any = {};

    if (params.include) {
      this.parseIncludes(params.include);
    }
  }
}

export default Manager;
