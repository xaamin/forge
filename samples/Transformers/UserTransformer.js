import { TransformerAbstract } from '@xaamin/forge';

import RolesTransfomer from "./RolesTransformer";

class UserTransformer extends TransformerAbstract {

    constructor() {
        super();

        this.availableIncludes = [
            'roles',
            'login-count'
        ];
    }

    transform(user) {
        return {
            id: user.id || '',
            last_login: user.last_login || '',
            mobile: user.mobile || '',
            name: user.name || '',
            organization_id: user.organization_id || '101',
            phone: user.phone || '',
            created_at: user.created_at,
            updated_at: user.updated_at
        };
    }

    includeRoles(user) {
        const roles = user.roles || [];

        return this.collection(roles, new RolesTransfomer(), 'roles');
    }

    includeLoginCount(user) {
        return user.login.count;
    }

}

export default UserTransformer;