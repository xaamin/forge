import { TransformerAbstract } from '@xaamin/forge';

import GroupTransfomer from './GroupTransfomer';

class RolesTransfomer extends TransformerAbstract {

    constructor() {
        super();

        this.availableIncludes = [
            'group'
        ];
    }

    transform(role) {
        return {
            id: role.id,
            name: role.name,
            description: role.description
        }
    }

    includeGroup(role) {
        const group = role.group ||  {};

        return this.item(group, new GroupTransfomer(), 'groups');
    }

}

export default RolesTransfomer;