import { TransformerAbstract } from '@xaamin/forge';

class GroupTransfomer extends TransformerAbstract {

    transform(group) {
        return {
            id: group.id || -1,
            name: group.slug || 'guest',
            description: group.description || 'Guests'
        }
    }

}

export default GroupTransfomer;