import Forge from '@xaamin/forge';
import { JsonApiSerializer } from '@xaamin/forge/build/module/serializers';
import UserTransformer from "./Transformers/UserTransformer";

const users = [
  {
    id: 1,
    name: 'Ben',
    email: 'ben@email.test',
    created_at: '2018-03-26 00:00:00',
    updated_at: '2018-03-26 00:00:01',
    login: {
      count: 10,
      last_login: '2018-03-27 09:53:22'
    },
    roles: [
      {
        id: Math.floor((Math.random() * 10) + 1),
        name: 'Admin users',
        group: {
          id: Math.floor((Math.random() * 10) + 1),
          slug: 'admin',
          description: 'Admin',
          created_at: '2018-03-26 00:00:00'
        },
        permissions: [{
          id: Math.floor((Math.random() * 10) + 1),
          name: 'Permission ' + Math.floor((Math.random() * 10) + 1),
        }]
      },
      {
        id: Math.floor((Math.random() * 10) + 1),
        slug: 'api',
        description: 'Api granted role',
        group: {
          id: Math.floor((Math.random() * 10) + 1),
          slug: 'api',
          description: 'API user',
          created_at: '2018-03-26 00:00:00'
        },
      },
      {
        id: Math.floor((Math.random() * 10) + 1),
        slug: 'guest',
        description: 'Guest user',
      }
    ]
  },
  {
    id: 2,
    name: 'Mayris',
    email: 'mayris@email.test',
    created_at: '2018-03-26 00:00:00',
    updated_at: '2018-03-26 00:00:01',
    login: {
      count: 8,
      last_login: '2018-03-27 09:53:22'
    },
    roles: [
      {
        id: Math.floor((Math.random() * 10) + 1),
        name: 'auditor',
        description: 'Auditor for management',
        group: {
          id: Math.floor((Math.random() * 10) + 1),
          slug: 'guest',
          description: 'Guest users',
          created_at: '2018-03-26 00:00:00'
        },
      }
    ]
  }
];

(async () => {
  const data = await Forge.make()
    .serializer('sld')
    .meta({
      limit: 100
    })
    .including(['roles.group'])
    .item(users[0], new UserTransformer())
    .toJSON();

  const dataAlt = await Forge.make()
    .meta({
      limit: 100
    })
    .withSerializer(new JsonApiSerializer('http://localhost'))
    .including(['roles.group'])
    .item(users[0], new UserTransformer(), 'users')
    .toJSON();

  console.log('users', users, data, dataAlt);
})();
